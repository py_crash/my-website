baseURL: "https://ferrario.me/"
languageCode: "en-us"
title: "py_crash's adventures"
theme: "tranquilpeak"

# Tranquilpeak defaults
paginate: 7
canonifyurls: true
taxonomies:
  tag: "tags"
  category: "categories"
  archive: "archives"

defaultContentLanguage: "en-us"
permalinks:
  post: "/:slug/"

author:
  name: Agustin Ferrario
  bio: >
    I'm a Chemical Engineer, a Linux user and a FOSS enthusiast.
    I write python from time to time, and I'm learning IoT.
    I wish to find a job where I can make the world a better place.
  job: Chemical Engineer
  location: Germany
  picture: images/A-Ferrario.png

# Sidebar configuration
menu:
  main:
    - weight: 1
      identifier: home
      name: Home
      pre: <i class="sidebar-button-icon fa fa-lg fa-home"></i>
      url: /
    - weight: 2
      identifier: categories
      name: Categories
      pre: <i class="sidebar-button-icon fa fa-lg fa-bookmark"></i>
      url: /categories
    - weight: 3
      identifier: tags
      name: Tags
      pre: <i class="sidebar-button-icon fa fa-lg fa-tags"></i>
      url: /tags
    - weight: 4
      identifier: archives
      name: Archives
      pre: <i class="sidebar-button-icon fa fa-lg fa-archive"></i>
      url: /archives
    - weight: 5
      identifier: about
      name: About
      pre: <i class="sidebar-button-icon fa fa-lg fa-question"></i>
      url: '/#about'
  links:
    - weight: 1
      identifier: gitlab
      name: GitLab
      pre: <i class="sidebar-button-icon fa fa-lg fa-gitlab"></i>
      url: "https://gitlab.com/py_crash"
    - weight: 2
      identifier: github
      name: GitHub
      pre: <i class="sidebar-button-icon fa fa-lg fa-github"></i>
      url: "https://github.com/py-crash"
    - weight: 3
      identifier: linkedin
      name: LinkedIn
      pre: <i class="sidebar-button-icon fa fa-lg fa-linkedin"></i>
      url: "https://www.linkedin.com/in/agustin-ferrario"
    - weight: 4
      identifier: twitter
      name: Twitter
      pre: <i class="sidebar-button-icon fa fa-lg fa-twitter"></i>
      url: "https://twitter.com/Py_Crash"
    # - weight: 5
    #   identifier: email
    #   name: Email me
    #   pre: <i class="sidebar-button-icon fa fa-lg fa-envelope"></i>
    #   url: "mailto: something@example.com" # FIXME
  misc:
    - weight: 1
      identifier: rss
      name: RSS
      pre: <i class="sidebar-button-icon fa fa-lg fa-rss"></i>
      url: /index.xml

params:
  dateFormat: "2 January 2006"
  keywords:
    - linux
    - FOSS
    - blog
    - Agustin Ferrario
  clearReading: true
  description: My adventures with linux and open source
  sidebarBehavior: 1
  coverImage: images/Mendoza.jpg
  imageGallery: true
  thumbnailImage: true
  thumbnailImagePosition: left
  autoThumbnailImage: true
  favicon: /favicon.ico
  hierarchicalCategories: false
  syntaxHighlighter: highlight.js

  # Added home button at the right side for the article view
  header:
    rightlink:
      class: ""
      icon: "fas fa-home"  # Make "" for profile pic
      url: "/"

  # Use to configure the address for the profile picture.
  # By default is /#about
  # sidebar:
  #   profile:
  #     url: /

  # Footer
  footer:
    copyright: >
      Agustin Ferrario.
      Under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank"> CC BY-SA</a>. <br/>
      Made with <i class="fa fa-heart"></i> in Argentina & Germany using <a href="https://gohugo.io/" target="_blank"> Hugo </a>
      and the <a href="https://themes.gohugo.io/hugo-tranquilpeak-theme/" target="_blank">
      tranquilpeak theme </a>.

  # Social Media Sharing
  sharingOptions:
    - name: Facebook
      icon: fa-facebook-official
      url: 'https://www.facebook.com/sharer/sharer.php?u=%s'
    - name: Twitter
      icon: fa-twitter
      url: 'https://twitter.com/intent/tweet?text=%s'
    - name: LinkedIn
      icon: fa-linkedin
      url: "https://www.linkedin.com/shareArticle?mini=true&url=%s"

  customCSS:
    - href: css/main.css

privacy:
  disqus:
    disable: true
  googleAnalytics:
    disable: true
  instagram:
    disable: true
  twitter:
    simple: true
    disableInlineCSS: true
  vimeo:
    disable: false
    simple: true
  youtube:
    disable: true
