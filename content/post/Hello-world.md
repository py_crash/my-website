---
title: "Hello World"
date: 2020-04-17T23:18:03-03:00
draft: false
thumbnailImage: /images/Hello-thumb.png
summary: >
  Hello there! This is my first post. Some history and the idea behind this blog.
  Read more to find out.
---

**Hello there!** ~~General Kenobi.~~ This is my blog. I wanted to create one for
a long time, but never took the time to do it. Maybe a pandemic and being without
a job after finishing university in February was the push I needed.

Here I will write things that I find interesting and will document my personal projects.
The idea is to write these things down, so I don't forget them and maybe someone will
find them useful or interesting.

I hope you enjoy this blog, and don't doubt to ping me on IRC (py_crash on
[freenode](https://freenode.net/) or [GeekShed](http://www.geekshed.net/)) or
[twitter](https://twitter.com/Py_Crash), if you have any comments about the posts
you find here.
