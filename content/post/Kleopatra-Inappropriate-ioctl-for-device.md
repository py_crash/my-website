---
title: 'Solving Kleopatra error "Inappropriate Ioctl for Device" on Gentoo'
date: 2020-11-19T21:53:22+01:00
categories:
- Linux
tags:
- Gentoo
- KDE
- gpg
- GnuPG
- pinentry
- eselect
keywords:
- tech
thumbnailImage: /images/Kleopatra-Gentoo-thumb.png
summary: >
  I like to use Kleopatra, the KDE crypto GUI, to manage my OpenPGP keys and
  sign documents. However I had a problem while running it under Gentoo. Here
  is the solution and the process I followed to find the solution.
---

I like using Kleopatra[^1] as a GUI for managing my OpenPGP keys and signing
(or encrypting documents). However, I had a problem on my Gentoo system when
trying to sign documents with it. Every time it will complain throwing the
following error:

```
gpg: signing failed: Inappropriate ioctl for device
```

After a bit of internet search I found a solution on [Stack Overflow] and on
[Github]. Any of those were related to Kleopatra specifically, but they
provided a workaround. Just running on a terminal the following commands:

```shell
$ GPG_TTY=$(tty)
$ export GPG_TTY
$ kleopatra
```

That allowed me to enter the password to the PGP key in the terminal window[^2]
where I had the program running. This worked for that CV that needed to be sent;
however it didn't make any sense as a permanent solution.

After some more searching, I found the [README] file that explains the
dependencies for Kleopatra for package maintainers in the source code [repo].
There I learned that the app uses pinentry[^3] for getting the password for gpg,
and was using the curses frontend (`pinentry-curses`) for that. So, when I
launched Kleopatra from my application launcher, pinetry didn't knew where to
run the curses interface to ask for my password.

I checked and I also had `pinentry-qt` installed[^4]. Then I wondered how do I
make that Kleopatra uses the qt-frontend instead of the curses one, so a new
window will pop up asking for my password instead of the terminal. Of course,
the solution was `eselect` (that's the gentoo way):

```shell
# eselect pinentry list
Available pinentry binary implementations:
  [1]   pinentry-gnome3
  [2]   pinentry-qt
  [3]   pinentry-gtk-2
  [4]   pinentry-qt4
  [5]   pinentry-curses *
  [6]   pinentry-tty

# eselect pinentry set 2
```

I hope this article it's helpful. I tried to write about the process for solving
my problem, so that it may be useful for you.

<!-- Footnotes -->
[^1]: Yes, it's a [KDE App](Kleopatra) (How did you notice? Because of the K?),
      however I tend to use it also in non-KDE desktops.
[^2]: It was a curses frontend, this will become important in a bit.
[^3]: If you are wondering just like me, it's read PIN Entry.
[^4]: To be precise, `app-crypt/pinentry` was emerged with the USE flag `qt5`.

<!-- Links -->
[Kleopatra]: https://apps.kde.org/en/kleopatra
[Stack Overflow]: https://stackoverflow.com/questions/57591432/gpg-signing-failed-inappropriate-ioctl-for-device-on-macos-with-maven
[Github]: https://github.com/keybase/keybase-issues/issues/2798
[README]: https://invent.kde.org/pim/kleopatra/-/blob/master/README.packagers
[repo]: https://invent.kde.org/pim/kleopatra