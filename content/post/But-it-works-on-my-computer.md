---
title: "But It Works on My Computer"
date: 2020-06-25T22:25:28-03:00
categories:
- Technology
tags:
- PEBCAK
- hugo
keywords:
- blog
- GitLab
thumbnailImage: /images/gopher-hero.svg
---

This is a short story about a PEBCAK [^1] moment (sort of) that I had while
updating my blog and a minor bug in [hugo-tranquilpeak-theme][tranquilpeak].

<!--more-->

On May 29th I decided that it was time to give some love to my repo, and added a
[LICENSE] file and a [README] so anyone could see a little more about the blog
when looking at the repo.

First, I added the LICENSE file, [committed][LICENSE] and push it to the repo. The
pipeline for rebuilding the site [failed][fail-1], however I didn't even look.

After that I added the README file, committed and push [again][README]. And then
again, the pipeline [failed][fail-2].

After this, I went on GitLab, and check the changes and saw the two failed pipelines.
That was really baffling, since I hadn't changed any files concerning Hugo.

I tried to build the blog on my computer running `hugo`, and it just worked. Now
I was really confused. It was one of those _"I don't know why is broken, it just
works on my computer"_ moments.

I looked again at the error shown on the logs, and searched around the internet,
and nothing helpful appeared.

After some time scratching my head (hopefully short, but I don't remember).
I checked GitLab's [Hugo container][container], and there was the problem. I was
using version 0.70.0 from the repo and GitLab had already updated to v0.71.1. 
After this, I installed the snap to check, and there it was: a bug on the theme
that I was use for my blog.

So, as a workaround, I reverted the GitLab CI to [version 0.70.0][revert] and
reported the bug to the [upstream project][tranquilpeak]. 24 hours later the bug
was fixed, I tested the changes and said thank you to the developer [^2], and
everything could go back to normal.



<!-- Footnotes -->
[^1]: Problem exists between chair and keyboard. http://catb.org/jargon/html/P/PEBKAC.html
[^2]: Please, always say thank you to all the projects maintainers, they are doing great work that makes your life better/easier.

<!-- Links -->
[LICENSE]: https://gitlab.com/py_crash/my-website/-/commit/7d8689dceceb0c18733fa866fa8a93979f342065
[README]: https://gitlab.com/py_crash/my-website/-/commit/83f534d80f2522d265a0f6ac83350d1c7d3a42d9
[fail-1]: https://gitlab.com/py_crash/my-website/-/pipelines/150746230
[fail-2]: https://gitlab.com/py_crash/my-website/-/pipelines/150761483
[container]: https://gitlab.com/pages/hugo/container_registry
[revert]: https://gitlab.com/py_crash/my-website/-/commit/cf8b18c07ed5e712d7e9c3d734d19ab252955d60
[tranquilpeak]: https://github.com/kakawait/hugo-tranquilpeak-theme/issues/429