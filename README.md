# ferrario.me

![Build status](https://gitlab.com/py_crash/my-website/badges/master/pipeline.svg)

---

This is the repository containing the source code for my website. You can visit
it in https://ferrario.me (or https://www.ferrario.me if you are _old school_).
**See you there!**

It's built using [Hugo]() and the [tranquilpeak theme](tranquilpeak).

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

  - [GitLab CI](#gitlab-ci)
  - [Building a local copy](#building-a-local-copy)
  - [Spotted a mistake? Have a comment?](#spotted-a-mistake-have-a-comment)
  - [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This static page is built using [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

For more information just check the [Hugo Website](hugo-deploy)

## Building a local copy

To work locally with this project, you'll have to follow the steps below:

1. Download this project and don't forget the submodules:
   ```shell
   git clone --recursive https://gitlab.com/py_crash/my-website.git
   ```
   Of course, you should fork it if you want to send me your changes

2. [Install][] Hugo
3. Preview your project: `hugo server`
4. Modify or add content in `content/`
5. Visualize your changes with `hugo server` (or `hugo server -D` if you have drafts)

Of course, if you want to know more read Hugo's [documentation][].

## Spotted a mistake? Have a comment?

If you spot a mistake don't be shy and open and issue, or contact me on [twitter]()
or IRC. You can also create a merge request if you feel up to it.

If you have any comments about any of the articles, ping me on [twitter]() or
the old IRC. More information about it in this [post](hello-world).

## License

All the source code for the website is under the GNU GPLv3, for more info see
[License](LICENSE).

All the articles written, everything in `content/`, are under [CC BY-SA](),
except for some materials that I took from other sites where it will be specified.

<!-- Links -->

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[hugo-deploy]: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[tranquilpeak]: https://themes.gohugo.io/hugo-tranquilpeak-theme/
[twitter]: https://twitter.com/py_crash
[hello-world]: https://ferrario.me/hello-world/
[CC BY-SA]: http://creativecommons.org/licenses/by-sa/4.0/